package main

import (
	"flag"
	"github.com/gin-gonic/gin"
	"log"
	"os"
	"strconv"
)

var (
	port = flag.Int("p", 8080, "listen port")
	help = flag.Bool("h", false, "print usage")
)

func main() {
	router := gin.Default()

	if err := router.Run(":" + strconv.Itoa(*port)); err != nil {
		log.Fatalln(err.Error())
	}
}

func init() {
	flag.Parse()
	if *help {
		flag.Usage()
		os.Exit(0)
	}
}
